from unittest import TestCase
from constants import GREETINGS
from entry import print_hi

class TestEntry(TestCase) : 
    def test_print_hi(self) :
        self.assertEqual(GREETINGS, print_hi())