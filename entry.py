from constants import GREETINGS

def print_hi() :
    return GREETINGS

def create_text_file() :
    with open('abc.txt','a') as file :
        file.write('This is test file \n This file is generated using python function.\n \
        This is a part of gitlab devops project')

if __name__ == '__main__' :
    import sys
    print(f'python version is : {sys.version}')
    create_text_file()
    print_hi()